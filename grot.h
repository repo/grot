/* This file is part of Grot.
   Copyright (C) 2009, 2010 Sergey Poznyakoff

   Grot is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Grot is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with grot.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <mysql.h>

extern char *grot_host;
extern char *grot_password;
extern int password_option;
extern unsigned grot_port;
extern char *grot_socket;
extern char *grot_user;
extern int verbose;
extern int dry_run_option;
extern int flush_logs_option;
extern size_t keep_count;

void verror(MYSQL *mysql, char *fmt, va_list ap);
void terror(int code, MYSQL *mysql, char *fmt, ...);
void read_options(int argc, char *argv[]);

#ifndef SYSCONFDIR
# define SYSCONFDIR "/etc"
#endif
#define DEFAULT_SYSTEM_CONFIG SYSCONFDIR "/grot.cfg"
#define DEFAULT_USER_CONFIG "~/.grot"

#define PACKAGE_STRING PACKAGE " " VERSION
#define PACKAGE_BUGREPORT "gray@gnu.org.ua"

